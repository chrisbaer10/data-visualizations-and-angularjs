# Data Visualizations and AngularJS

My code samples from "Data Visualizations and AngularJS" talk at [No Fluff Just Stuff](https://nofluffjuststuff.com). There are 2 parts to this talk - [Part I](https://nofluffjuststuff.com/topics/data_visualizations_and_angularjs__part_1) and [Part II](https://nofluffjuststuff.com/topics/data_visualizations_and_angularjs__part_2)

**NOTE** This repo will see massive, and often destructive changes over time. Preferably fork this repo vs. cloning. 

## Set up

This project uses a [Yeoman](http://yeoman.io/) generator, namely [generator-gulp-angular](https://github.com/Swiip/generator-gulp-angular). Please see it's [README](https://github.com/Swiip/generator-gulp-angular/blob/master/README.md#install) to se what you will need installed.

Then simply run 

```
gulp serve
```

in the root of this project. Your browser should automatically open up to a new tab with the application running. If it does not simply click [here](http://localhost:3000/#/).

Have fun. 

## Credits

The primary [demo](http://localhost:3000/#/demo) was inspired by this fabulous visualization of [Gay rights in the US, state by state](http://www.theguardian.com/world/interactive/2012/may/08/gay-rights-united-states) by The Guardian. 

## License

Copyright (c) 2015 Raju Gandhi

Distributed under the MIT License
