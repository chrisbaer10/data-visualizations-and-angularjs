(function() {
  'use strict';

  angular
    .module('dataVisualizationsAndAngularJs')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
